<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\components;

use Yii;

class Notify {

    public $message;
    // public $successMessage = ' Proses kemaskini data berjaya.';
    // public $failMessage = ' Proses kemaskini data gagal.';
    public $duration;

    public function fail($message = ' Proses kemaskini data gagal.', $duration = 8000) {
        return Yii::$app->session->setFlash('error', [
                    'type' => 'danger',
                    'duration' => $duration,
                    'icon' => 'glyphicon glyphicon-remove-sign',
                    'title' => ' GAGAL.',
                    'message' => $message
        ]);
    }

    public function success($message = ' Proses kemaskini data berjaya.', $duration = 3000) {
        return Yii::$app->session->setFlash('success', [
                    'type' => 'success',
                    'duration' => $duration,
                    'icon' => 'glyphicon glyphicon-check-sign',
                    'title' => ' BERJAYA.',
                    'message' => $message
        ]);
    }

    public function info($message, $duration = 3000) {
        return Yii::$app->session->setFlash('info', [
                    'type' => 'info',
                    'duration' => $duration,
                    'icon' => 'glyphicon glyphicon-info-sign',
                    'title' => ' INFO.',
                    'message' => $message
        ]);
    }

    public function warning($message, $duration = 3000) {
        return Yii::$app->session->setFlash('warning', [
                    'type' => 'warning',
                    'duration' => $duration,
                    'icon' => 'glyphicon glyphicon-exclamation-mark',
                    'title' => ' AMARAN.',
                    'message' => $message
        ]);
    }

}
