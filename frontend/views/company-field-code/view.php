<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model frontend\models\CompanyFieldCode */

$this->title = $model->code_id;
$this->params['breadcrumbs'][] = ['label' => 'Company Field Code', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-field-code-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Company Field Code'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', 'code_id' => $model->code_id, 'company_id' => $model->company_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'code_id' => $model->code_id, 'company_id' => $model->company_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        [
            'attribute' => 'code.name',
            'label' => 'Code',
        ],
        [
            'attribute' => 'company.name',
            'label' => 'Company',
        ],
        ['attribute' => 'status', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    <div class="row">
        <h4>Company<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnCompany = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        ['attribute' => 'status', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->company,
        'attributes' => $gridColumnCompany    ]);
    ?>
    <div class="row">
        <h4>FieldCode<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnFieldCode = [
        ['attribute' => 'id', 'visible' => false],
        'code',
        'name',
        ['attribute' => 'status', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->code,
        'attributes' => $gridColumnFieldCode    ]);
    ?>
</div>
