<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model frontend\models\CompanyFieldCode */

$this->title = $model->code_id;
$this->params['breadcrumbs'][] = ['label' => 'Company Field Code', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-field-code-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Company Field Code'.' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        [
                'attribute' => 'code.name',
                'label' => 'Code'
            ],
        [
                'attribute' => 'company.name',
                'label' => 'Company'
            ],
        ['attribute' => 'status', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>
