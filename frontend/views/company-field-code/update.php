<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\CompanyFieldCode */

$this->title = 'Update Company Field Code: ' . ' ' . $model->code_id;
$this->params['breadcrumbs'][] = ['label' => 'Company Field Code', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->code_id, 'url' => ['view', 'code_id' => $model->code_id, 'company_id' => $model->company_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="company-field-code-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
