<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model frontend\models\CompanyFieldCode */

?>
<div class="company-field-code-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->code_id) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        [
            'attribute' => 'code.name',
            'label' => 'Code',
        ],
        [
            'attribute' => 'company.name',
            'label' => 'Company',
        ],
        ['attribute' => 'status', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>