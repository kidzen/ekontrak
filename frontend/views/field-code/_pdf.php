<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model frontend\models\FieldCode */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Field Code', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="field-code-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Field Code'.' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'code',
        'name',
        ['attribute' => 'status', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerCompanyFieldCode->totalCount){
    $gridColumnCompanyFieldCode = [
        ['class' => 'yii\grid\SerialColumn'],
                [
                'attribute' => 'company.name',
                'label' => 'Company'
            ],
        ['attribute' => 'status', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerCompanyFieldCode,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode('Company Field Code'),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnCompanyFieldCode
    ]);
}
?>
    </div>
</div>
