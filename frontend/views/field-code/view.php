<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model frontend\models\FieldCode */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Field Code', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="field-code-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Field Code'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'code',
        'name',
        ['attribute' => 'status', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    
    <div class="row">
<?php
if($providerCompanyFieldCode->totalCount){
    $gridColumnCompanyFieldCode = [
        ['class' => 'yii\grid\SerialColumn'],
                        [
                'attribute' => 'company.name',
                'label' => 'Company'
            ],
            ['attribute' => 'status', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerCompanyFieldCode,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-company-field-code']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Company Field Code'),
        ],
        'export' => false,
        'columns' => $gridColumnCompanyFieldCode
    ]);
}
?>

    </div>
</div>
