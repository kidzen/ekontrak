<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\FieldCode */

$this->title = 'Update Field Code: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Field Code', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="field-code-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
