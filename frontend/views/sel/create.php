<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\Sel */

$this->title = 'Create Sel';
$this->params['breadcrumbs'][] = ['label' => 'Sel', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sel-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
