<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\Progress */

$this->title = 'Update Progress: ' . ' ' . $model->progress;
$this->params['breadcrumbs'][] = ['label' => 'Progress', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->progress, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="progress-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
