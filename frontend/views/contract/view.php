<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model frontend\models\Contract */

$this->title = $model->contract_no;
$this->params['breadcrumbs'][] = ['label' => 'Contract', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contract-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Contract'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'category.name',
            'label' => 'Category',
        ],
        'contract_no',
        [
            'attribute' => 'meeting.title',
            'label' => 'Meeting',
        ],
        'revenue',
        'start_date',
        'end_date',
        ['attribute' => 'status', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    <div class="row">
        <h4>Category<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnCategory = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'description',
        ['attribute' => 'status', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->category,
        'attributes' => $gridColumnCategory    ]);
    ?>
    <div class="row">
        <h4>Meeting<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnMeeting = [
        ['attribute' => 'id', 'visible' => false],
        'title',
        'timestamp',
        ['attribute' => 'status', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->meeting,
        'attributes' => $gridColumnMeeting    ]);
    ?>
    
    <div class="row">
<?php
if($providerContractGrant->totalCount){
    $gridColumnContractGrant = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            [
                'attribute' => 'meeting.title',
                'label' => 'Meeting'
            ],
                        [
                'attribute' => 'company.name',
                'label' => 'Company'
            ],
            'type',
            'revenue',
            'start_date',
            'end_date',
            ['attribute' => 'status', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerContractGrant,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-contract-grant']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Contract Grant'),
        ],
        'export' => false,
        'columns' => $gridColumnContractGrant
    ]);
}
?>

    </div>
</div>
