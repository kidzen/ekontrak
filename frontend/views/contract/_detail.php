<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model frontend\models\Contract */

?>
<div class="contract-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->contract_no) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'category.name',
            'label' => 'Category',
        ],
        'contract_no',
        [
            'attribute' => 'meeting.title',
            'label' => 'Meeting',
        ],
        'revenue',
        'start_date',
        'end_date',
        ['attribute' => 'status', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>