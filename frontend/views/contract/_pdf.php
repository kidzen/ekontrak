<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model frontend\models\Contract */

$this->title = $model->contract_no;
$this->params['breadcrumbs'][] = ['label' => 'Contract', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contract-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Contract'.' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
                'attribute' => 'category.name',
                'label' => 'Category'
            ],
        'contract_no',
        [
                'attribute' => 'meeting.title',
                'label' => 'Meeting'
            ],
        'revenue',
        'start_date',
        'end_date',
        ['attribute' => 'status', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerContractGrant->totalCount){
    $gridColumnContractGrant = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        'grant_no',
                [
                'attribute' => 'company.name',
                'label' => 'Company'
            ],
        'type',
        'revenue',
        'start_date',
        'end_date',
        ['attribute' => 'status', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerContractGrant,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode('Contract Grant'),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnContractGrant
    ]);
}
?>
    </div>
</div>
