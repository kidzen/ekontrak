<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model frontend\models\JobGrant */

$this->title = $model->job_id;
$this->params['breadcrumbs'][] = ['label' => 'Job Grant', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="job-grant-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Job Grant'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', 'job_id' => $model->job_id, 'grant_id' => $model->grant_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'job_id' => $model->job_id, 'grant_id' => $model->grant_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        [
            'attribute' => 'job.name',
            'label' => 'Job',
        ],
        [
            'attribute' => 'grant.id',
            'label' => 'Grant',
        ],
        ['attribute' => 'status', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    <div class="row">
        <h4>Job<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnJob = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'description',
        'start_date',
        'end_date',
        ['attribute' => 'status', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->job,
        'attributes' => $gridColumnJob    ]);
    ?>
    <div class="row">
        <h4>ContractGrant<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnContractGrant = [
        ['attribute' => 'id', 'visible' => false],
        'meeting_id',
        'contract_id',
        'company_id',
        'type',
        'revenue',
        'start_date',
        'end_date',
        ['attribute' => 'status', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->grant,
        'attributes' => $gridColumnContractGrant    ]);
    ?>
</div>
