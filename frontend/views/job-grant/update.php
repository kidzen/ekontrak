<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\JobGrant */

$this->title = 'Update Job Grant: ' . ' ' . $model->job_id;
$this->params['breadcrumbs'][] = ['label' => 'Job Grant', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->job_id, 'url' => ['view', 'job_id' => $model->job_id, 'grant_id' => $model->grant_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="job-grant-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
