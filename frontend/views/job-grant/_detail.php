<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model frontend\models\JobGrant */

?>
<div class="job-grant-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->job_id) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        [
            'attribute' => 'job.name',
            'label' => 'Job',
        ],
        [
            'attribute' => 'grant.id',
            'label' => 'Grant',
        ],
        ['attribute' => 'status', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>