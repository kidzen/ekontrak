<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\JobGrant */

$this->title = 'Create Job Grant';
$this->params['breadcrumbs'][] = ['label' => 'Job Grant', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="job-grant-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
