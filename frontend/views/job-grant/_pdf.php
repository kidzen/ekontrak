<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model frontend\models\JobGrant */

$this->title = $model->job_id;
$this->params['breadcrumbs'][] = ['label' => 'Job Grant', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="job-grant-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Job Grant'.' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        [
                'attribute' => 'job.name',
                'label' => 'Job'
            ],
        [
                'attribute' => 'grant.grant_no',
                'label' => 'Grant'
            ],
        ['attribute' => 'status', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>
