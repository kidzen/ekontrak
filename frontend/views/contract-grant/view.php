<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model frontend\models\ContractGrant */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Contract Grant', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contract-grant-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Contract Grant'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'meeting.title',
            'label' => 'Meeting',
        ],
        [
            'attribute' => 'contract.contract_no',
            'label' => 'Contract',
        ],
        [
            'attribute' => 'company.name',
            'label' => 'Company',
        ],
        'type',
        'revenue',
        'start_date',
        'end_date',
        ['attribute' => 'status', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    <div class="row">
        <h4>Company<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnCompany = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        ['attribute' => 'status', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->company,
        'attributes' => $gridColumnCompany    ]);
    ?>
    <div class="row">
        <h4>Meeting<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnMeeting = [
        ['attribute' => 'id', 'visible' => false],
        'title',
        'timestamp',
        ['attribute' => 'status', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->meeting,
        'attributes' => $gridColumnMeeting    ]);
    ?>
    <div class="row">
        <h4>Contract<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnContract = [
        ['attribute' => 'id', 'visible' => false],
        'category_id',
        'contract_no',
        [
            'attribute' => 'meeting.title',
            'label' => 'Meeting',
        ],
        'revenue',
        'start_date',
        'end_date',
        ['attribute' => 'status', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->contract,
        'attributes' => $gridColumnContract    ]);
    ?>
    
    <div class="row">
<?php
if($providerJobGrant->totalCount){
    $gridColumnJobGrant = [
        ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'job.name',
                'label' => 'Job'
            ],
                        ['attribute' => 'status', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerJobGrant,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-job-grant']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Job Grant'),
        ],
        'export' => false,
        'columns' => $gridColumnJobGrant
    ]);
}
?>

    </div>
</div>
