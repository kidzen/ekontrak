<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model frontend\models\ContractGrant */

$this->title = $model->grant_no;
$this->params['breadcrumbs'][] = ['label' => 'Contract Grant', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contract-grant-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Contract Grant'.' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'grant_no',
        [
                'attribute' => 'contract.contract_no',
                'label' => 'Contract'
            ],
        [
                'attribute' => 'company.name',
                'label' => 'Company'
            ],
        'type',
        'revenue',
        'start_date',
        'end_date',
        ['attribute' => 'status', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerJobGrant->totalCount){
    $gridColumnJobGrant = [
        ['class' => 'yii\grid\SerialColumn'],
        [
                'attribute' => 'job.name',
                'label' => 'Job'
            ],
                ['attribute' => 'status', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerJobGrant,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode('Job Grant'),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnJobGrant
    ]);
}
?>
    </div>
</div>
