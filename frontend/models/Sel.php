<?php

namespace frontend\models;

use Yii;
use \frontend\models\base\Sel as BaseSel;

/**
 * This is the model class for table "sel".
 */
class Sel extends BaseSel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['status', 'deleted'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at', 'created_by', 'updated_by', 'deleted_by'], 'safe'],
            [['name', 'description'], 'string', 'max' => 255],
            [['name'], 'unique']
        ]);
    }
	
}
