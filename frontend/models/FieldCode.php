<?php

namespace frontend\models;

use Yii;
use \frontend\models\base\FieldCode as BaseFieldCode;

/**
 * This is the model class for table "field_code".
 */
class FieldCode extends BaseFieldCode
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['code', 'status', 'deleted'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at', 'created_by', 'updated_by', 'deleted_by'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['code'], 'unique'],
            [['name'], 'unique']
        ]);
    }
	
}
