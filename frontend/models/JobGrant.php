<?php

namespace frontend\models;

use Yii;
use \frontend\models\base\JobGrant as BaseJobGrant;

/**
 * This is the model class for table "job_grant".
 */
class JobGrant extends BaseJobGrant
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['job_id', 'grant_id'], 'required'],
            [['job_id', 'grant_id', 'status', 'deleted'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at', 'created_by', 'updated_by', 'deleted_by'], 'safe']
        ]);
    }
	
}
