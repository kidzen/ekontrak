<?php

namespace frontend\models\query;

/**
 * This is the ActiveQuery class for [[\frontend\models\query\Contract]].
 *
 * @see \frontend\models\query\Contract
 */
class ContractQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \frontend\models\query\Contract[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \frontend\models\query\Contract|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
