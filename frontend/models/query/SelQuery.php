<?php

namespace frontend\models\query;

/**
 * This is the ActiveQuery class for [[\frontend\models\query\Sel]].
 *
 * @see \frontend\models\query\Sel
 */
class SelQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \frontend\models\query\Sel[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \frontend\models\query\Sel|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
