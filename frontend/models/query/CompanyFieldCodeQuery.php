<?php

namespace frontend\models\query;

/**
 * This is the ActiveQuery class for [[\frontend\models\query\CompanyFieldCode]].
 *
 * @see \frontend\models\query\CompanyFieldCode
 */
class CompanyFieldCodeQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \frontend\models\query\CompanyFieldCode[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \frontend\models\query\CompanyFieldCode|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
