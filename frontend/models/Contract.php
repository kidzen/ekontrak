<?php

namespace frontend\models;

use Yii;
use \frontend\models\base\Contract as BaseContract;

/**
 * This is the model class for table "contract".
 */
class Contract extends BaseContract
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['category_id', 'meeting_id', 'status', 'deleted'], 'integer'],
            [['revenue'], 'number'],
            [['start_date', 'end_date', 'created_at', 'updated_at', 'deleted_at', 'created_by', 'updated_by', 'deleted_by'], 'safe'],
            [['contract_no'], 'string', 'max' => 255],
            [['contract_no'], 'unique']
        ]);
    }
	
}
