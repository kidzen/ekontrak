<?php

namespace frontend\models;

use Yii;
use \frontend\models\base\Settings as BaseSettings;

/**
 * This is the model class for table "settings".
 */
class Settings extends BaseSettings
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['key', 'value'], 'required'],
            [['key', 'value'], 'string', 'max' => 255],
            [['key'], 'unique']
        ]);
    }
	
}
