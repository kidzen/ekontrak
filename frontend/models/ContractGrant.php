<?php

namespace frontend\models;

use Yii;
use \frontend\models\base\ContractGrant as BaseContractGrant;

/**
 * This is the model class for table "contract_grant".
 */
class ContractGrant extends BaseContractGrant
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['meeting_id', 'contract_id', 'company_id', 'status', 'deleted'], 'integer'],
            [['revenue'], 'number'],
            [['start_date', 'end_date', 'created_at', 'updated_at', 'deleted_at', 'created_by', 'updated_by', 'deleted_by'], 'safe'],
            [['type'], 'string', 'max' => 255]
        ]);
    }

}
