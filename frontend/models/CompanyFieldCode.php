<?php

namespace frontend\models;

use Yii;
use \frontend\models\base\CompanyFieldCode as BaseCompanyFieldCode;

/**
 * This is the model class for table "company_field_code".
 */
class CompanyFieldCode extends BaseCompanyFieldCode
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['code_id', 'company_id'], 'required'],
            [['code_id', 'company_id', 'status', 'deleted'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at', 'created_by', 'updated_by', 'deleted_by'], 'safe']
        ]);
    }
	
}
