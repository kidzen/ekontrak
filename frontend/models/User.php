<?php

namespace frontend\models;

use Yii;
use \frontend\models\base\User as BaseUser;

/**
 * This is the model class for table "user".
 */
class User extends BaseUser
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['username', 'role_id', 'auth_key', 'password_hash'], 'required'],
            [['role_id', 'status', 'deleted'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at', 'created_by', 'updated_by', 'deleted_by'], 'safe'],
            [['username', 'password_hash', 'password_reset_token', 'email'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['username'], 'unique'],
            [['username'], 'unique'],
            [['password_reset_token'], 'unique'],
            [['email'], 'unique']
        ]);
    }
	
}
