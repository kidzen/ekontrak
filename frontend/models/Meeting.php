<?php

namespace frontend\models;

use Yii;
use \frontend\models\base\Meeting as BaseMeeting;

/**
 * This is the model class for table "meeting".
 */
class Meeting extends BaseMeeting
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['timestamp', 'created_at', 'updated_at', 'deleted_at', 'created_by', 'updated_by', 'deleted_by'], 'safe'],
            [['status', 'deleted'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['title'], 'unique']
        ]);
    }
	
}
