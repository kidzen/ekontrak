<?php

namespace frontend\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "contract_grant".
 *
 * @property integer $id
 * @property integer $meeting_id
 * @property integer $contract_id
 * @property integer $company_id
 * @property string $type
 * @property double $revenue
 * @property string $start_date
 * @property string $end_date
 * @property integer $status
 * @property integer $deleted
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property string $created_by
 * @property string $updated_by
 * @property string $deleted_by
 *
 * @property \frontend\models\Company $company
 * @property \frontend\models\Meeting $meeting
 * @property \frontend\models\Contract $contract
 * @property \frontend\models\JobGrant[] $jobGrants
 * @property \frontend\models\Job[] $jobs
 */
class ContractGrant extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    private $_rt_softdelete;
    private $_rt_softrestore;

    public function __construct(){
        parent::__construct();
        $this->_rt_softdelete = [
            'deleted_by' => \Yii::$app->user->id,
            'deleted_at' => date('Y-m-d H:i:s'),
        ];
        $this->_rt_softrestore = [
            'deleted_by' => 0,
            'deleted_at' => date('Y-m-d H:i:s'),
        ];
    }

    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'company',
            'meeting',
            'contract',
            'jobGrants',
            'jobs'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['meeting_id', 'contract_id', 'company_id', 'status', 'deleted'], 'integer'],
            [['revenue'], 'number'],
            [['start_date', 'end_date', 'created_at', 'updated_at', 'deleted_at', 'created_by', 'updated_by', 'deleted_by'], 'safe'],
            [['type'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contract_grant';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'meeting_id' => 'Meeting ID',
            'contract_id' => 'Contract ID',
            'company_id' => 'Company ID',
            'type' => 'Type',
            'revenue' => 'Revenue',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'status' => 'Status',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(\frontend\models\Company::className(), ['id' => 'company_id'])->inverseOf('contractGrants');
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeeting()
    {
        return $this->hasOne(\frontend\models\Meeting::className(), ['id' => 'meeting_id'])->inverseOf('contractGrants')->inverseOf('contractGrants');
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContract()
    {
        return $this->hasOne(\frontend\models\Contract::className(), ['id' => 'contract_id'])->inverseOf('contractGrants');
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJobGrants()
    {
        return $this->hasMany(\frontend\models\JobGrant::className(), ['grant_id' => 'id'])->inverseOf('grant');
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJobs()
    {
        return $this->hasMany(\frontend\models\Job::className(), ['id' => 'job_id'])->viaTable('job_grant', ['grant_id' => 'id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    /**
     * The following code shows how to apply a default condition for all queries:
     *
     * ```php
     * class Customer extends ActiveRecord
     * {
     *     public static function find()
     *     {
     *         return parent::find()->where(['deleted' => false]);
     *     }
     * }
     *
     * // Use andWhere()/orWhere() to apply the default condition
     * // SELECT FROM customer WHERE `deleted`=:deleted AND age>30
     * $customers = Customer::find()->andWhere('age>30')->all();
     *
     * // Use where() to ignore the default condition
     * // SELECT FROM customer WHERE age>30
     * $customers = Customer::find()->where('age>30')->all();
     * ```
     */

    /**
     * @inheritdoc
     * @return \frontend\models\query\ContractGrantQuery the active query used by this AR class.
     */
    public static function find()
    {
        $query = new \frontend\models\query\ContractGrantQuery(get_called_class());
        return $query->where(['contract_grant.deleted_by' => 0]);
    }
}
