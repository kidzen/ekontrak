<?php

namespace frontend\models;

use Yii;
use \frontend\models\base\Job as BaseJob;

/**
 * This is the model class for table "job".
 */
class Job extends BaseJob
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['start_date', 'end_date', 'created_at', 'updated_at', 'deleted_at', 'created_by', 'updated_by', 'deleted_by'], 'safe'],
            [['status', 'deleted'], 'integer'],
            [['name', 'description'], 'string', 'max' => 255],
            [['name'], 'unique']
        ]);
    }
	
}
