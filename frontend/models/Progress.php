<?php

namespace frontend\models;

use Yii;
use \frontend\models\base\Progress as BaseProgress;

/**
 * This is the model class for table "progress".
 */
class Progress extends BaseProgress
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['job_id', 'status', 'deleted'], 'integer'],
            [['progress', 'cost'], 'number'],
            [['start_date', 'end_date', 'created_at', 'updated_at', 'deleted_at', 'created_by', 'updated_by', 'deleted_by'], 'safe'],
            [['description'], 'string', 'max' => 255]
        ]);
    }
	
}
