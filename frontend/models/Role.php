<?php

namespace frontend\models;

use Yii;
use \frontend\models\base\Role as BaseRole;

/**
 * This is the model class for table "role".
 */
class Role extends BaseRole
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['status', 'deleted'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at', 'created_by', 'updated_by', 'deleted_by'], 'safe'],
            [['name', 'description'], 'string', 'max' => 255],
            [['name'], 'unique']
        ]);
    }
	
}
