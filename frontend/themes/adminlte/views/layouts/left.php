<?php
use yii\helpers\Url;
// $data = Yii::$app->db->getSchema()->getTableNames();
// $data = Yii::$app->request->hostInfo;
// var_dump($data);
// die();

?>
<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div style="text-align: center">
                <img src="<?= Yii::getAlias('@web').Url::to('/images/MPSP.png') ?>" style="width: 100%" alt="Mohor_rasmi_Majlis_Perbandaran_Seberang_Perai"/>
                <!--<img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" style="width: 200px" alt="TUDM"/>-->
            </div>
        </div>


        <!-- search form -->
<!--         <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
    -->        <!-- /.search form -->

    <?= dmstr\widgets\Menu::widget(
        [
            'options' => ['class' => 'sidebar-menu','data-widget' => 'tree'], //for version 2.5
            // 'options' => ['class' => 'sidebar-menu'],
            'items' => [
                ['label' => 'Menu', 'options' => ['class' => 'header']],
                [
                    'label' => 'Log Masuk', 'icon' => 'share', 'url' => ['/site/login'],
                    'visible' => Yii::$app->user->isGuest,
                ],
                [
                    'label' => 'Permohonan', 'icon' => 'stack-overflow',
                    // 'visible' => !Yii::$app->user->isGuest,
                    'items' => [
                        ['label' => 'register contract', 'icon' => 'calendar-check-o', 'url' => ['/contract/create']],
                        ['label' => 'register job', 'icon' => 'calendar-check-o', 'url' => ['/job/create']],
                        ['label' => 'award-grant', 'icon' => 'calendar-check-o', 'url' => ['/contract-grant/create']]                    ]
                ],
                [
                    'label' => 'Rekod', 'icon' => 'stack-overflow',
                    // 'visible' => !Yii::$app->user->isGuest,
                    'items' => [
                        ['label' => 'job', 'icon' => 'calendar-check-o', 'url' => ['/job/index']],
                        ['label' => 'contract', 'icon' => 'calendar-check-o', 'url' => ['/contract/index']],
                        ['label' => 'contract-grant', 'icon' => 'calendar-check-o', 'url' => ['/contract-grant/index']],
                        ['label' => 'progress', 'url' => ['/progress/index']],
                    ]
                ],
                [
                    'label' => 'Konfigurasi Sistem', 'icon' => 'dashboard',
                        // 'visible'=>false,
                    // 'visible' => Yii::$app->user->isAdmin,
                    'items' => [
                        ['label' => 'category', 'icon' => 'balance-scale', 'url' => ['/category/index']],
                        ['label' => 'company', 'icon' => 'balance-scale', 'url' => ['/company/index']],
                        ['label' => 'field-code', 'icon' => 'balance-scale', 'url' => ['/field-code/index']],
                        ['label' => 'meeting', 'icon' => 'balance-scale', 'url' => ['/meeting/index']],
                        ['label' => 'role', 'icon' => 'balance-scale', 'url' => ['/role/index']],
                        // ['label' => 'job-grant', 'url' => ['/job-grant/index']],
                        ['label' => 'settings', 'url' => ['/settings/index']],
                        // ['label' => 'company-field-code', 'url' => ['/company-field-code/index']],
                        ['label' => 'user', 'url' => ['/user/index']],
                    ],
                ],
                [
                    'label' => 'Pentadbiran', 'icon' => 'dashboard',
                        'visible'=>false,
                    // 'visible' => Yii::$app->user->isUserDepartment || Yii::$app->user->isAdminDepartment,
                    'items' => [
                        ['label' => 'Pengguna', 'icon' => 'users', 'url' => ['/user/view','id'=>Yii::$app->user->id],],
                    ]
                ],
                [
                    'label' => 'Pentadbiran', 'icon' => 'dashboard',
                        'visible'=>false,
                    // 'visible' => Yii::$app->user->isAdmin || Yii::$app->user->isAdminJuu,
                    'items' => [
                        // ['label' => 'Profil', 'icon' => 'users', 'url' => ['/user/view','id'=>Yii::$app->user->id],],
                        ['label' => 'Pengguna', 'icon' => 'user', 'url' => ['/profile/index']],
                        ['label' => 'Pengguna Sistem', 'icon' => 'users', 'url' => ['/user/index'],],
                        // ['label' => 'Akses', 'icon' => 'lock', 'url' => ['/role/index'],'visible' => Yii::$app->user->isAdmin],
                    ]
                ],
                [
                    'visible'=>true,
                    'active'=>false,
                    'label' => 'Dev tools',
                    'icon' => 'share',
                        // 'url' => '#',
                    'items' => [
                        [
                            'label' => 'Database',
                            'icon' => 'database',
                            'items' => [
                                ['label' => 'category', 'icon' => 'database', 'url' => ['/category/index']],
                                ['label' => 'company', 'icon' => 'database', 'url' => ['/company/index']],
                                ['label' => 'field-code', 'icon' => 'database', 'url' => ['/field-code/index']],
                                ['label' => 'company-field-code', 'icon' => 'database', 'url' => ['/company-field-code/index']],
                                ['label' => 'contract', 'icon' => 'database', 'url' => ['/contract/index']],
                                ['label' => 'contract-grant', 'icon' => 'database', 'url' => ['/contract-grant/index']],
                                ['label' => 'job', 'icon' => 'database', 'url' => ['/job/index']],
                                ['label' => 'job-grant', 'icon' => 'database', 'url' => ['/job-grant/index']],
                                ['label' => 'meeting', 'icon' => 'database', 'url' => ['/meeting/index']],
                                ['label' => 'progress', 'icon' => 'database', 'url' => ['/progress/index']],
                                ['label' => 'role', 'icon' => 'database', 'url' => ['/role/index']],
                                ['label' => 'settings', 'icon' => 'database', 'url' => ['/settings/index']],
                                ['label' => 'user', 'icon' => 'database', 'url' => ['/user/index']],
                            ],
                        ],
                        ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii'],],
                        ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug'],],
                    ],
                ],
                ['label' => 'Manual Sistem', 'icon' => 'info', 'url' => ['/site/info'],'visible'=>false,],
            ],
        ]
        ) ?>

    </section>

</aside>
