<?php
use yii\helpers\Html;

// var_dump(Yii::$app->params['themes']);die();
/* @var $this \yii\web\View */
/* @var $content string */
// var_dump(Yii::$app->request->hostInfo.Yii::$app->request->baseUrl);die; //faster
// var_dump(Yii::getAlias('@web').\yii\helpers\Url::to('/images/MPSP.png'));die;
$this->registerCss('
    .img-layout{
    // background-image:url("'.Yii::$app->request->hostInfo.Yii::$app->request->baseUrl.'/images/kontrak3.jpg");
        background-image:url("'.Yii::getAlias('@web').\yii\helpers\Url::to('/images/kontrak3.jpg').'");
    // background-size:120%;
        background-size:100%;
    }
    @media only screen
    and (min-device-width : 320px)
    and (max-device-width : 480px){
        .large-screen { display: none;}
    }'
);
        // var_dump(Yii::$app->name);die;


if (Yii::$app->controller->action->id === 'login') {
/**
 * Do not use this code in your template. Remove it.
 * Instead, use the code  $this->layout = '//main-login'; in your controller.
 */
echo $this->render(
    'main-login',
    ['content' => $content]
);
} else {

    if (class_exists('backend\assets\AppAsset')) {
        backend\assets\AppAsset::register($this);
    } else {
        app\assets\AppAsset::register($this);
    }

    dmstr\web\AdminLteAsset::register($this);
    $directoryAsset = Yii::$app->assetManager->getPublishedUrl('@vendor/almasaeed2010/adminlte/dist');
    ?>
    <?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode(Yii::$app->name) ?></title>
        <?php $this->head() ?>
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <?php $this->beginBody() ?>
        <div class="wrapper">

            <?= $this->render(
                'header.php',
                ['directoryAsset' => $directoryAsset]
                ) ?>

            <?= $this->render(
                'left.php',
                ['directoryAsset' => $directoryAsset]
            )
            ?>

            <?= $this->render(
                'content.php',
                ['content' => $content, 'directoryAsset' => $directoryAsset]
                ) ?>

            </div>

            <?php $this->endBody() ?>
        </body>
        </html>
        <?php $this->endPage() ?>
        <?php } ?>
