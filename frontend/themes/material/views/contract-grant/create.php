<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\ContractGrant */

$this->title = 'Create Contract Grant';
$this->params['breadcrumbs'][] = ['label' => 'Contract Grant', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contract-grant-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
