<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\FieldCode */

$this->title = 'Create Field Code';
$this->params['breadcrumbs'][] = ['label' => 'Field Code', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="field-code-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
