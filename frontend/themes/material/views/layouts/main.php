<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use ramosisw\CImaterial\widgets\Alert;
use ramosisw\CImaterial\web\MaterialAsset;

MaterialAsset::register($this);
$inlineCss = <<<CSS
pre.prettyprint{
    background-color: #eee;
    border: 0px;
    margin-bottom: 60px;
    margin-top: 30px;
    padding: 20px;
    text-align: left;
}
.atv, .str{
    color: #05AE0E;
}
.tag, .pln, .kwd{
    color: #3472F7;
}
.atn{
    color: #2C93FF;
}
.pln{
    color: #333;
}
.com{
    color: #999;
}
.space-top{
    margin-top: 50px;
}
.area-line{
    border: 1px solid #999;
    border-left: 0;
    border-right: 0;
    color: #666;
    display: block;
    margin-top: 20px;
    padding: 8px 0;
    text-align: center;
}
.area-line a{
    color: #666;
}
.container-fluid{
    padding-right: 15px;
    padding-left: 15px;
}

CSS;
$this->registerCss($inlineCss);
$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@vendor/almasaeed2010/adminlte/dist');
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png" />
    <link rel="icon" type="image/png" href="../assets/img/favicon.png" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=0"> -->
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="components-page">
    <?php $this->beginBody() ?>

    <div class="wrapper">
        <?= $this->render(
            'left.php',
            ['directoryAsset' => $directoryAsset]
        )
        ?>
        <div class="main-panel">
            <?= $this->render(
                'header.php',
                ['directoryAsset' => $directoryAsset]
            )
            ?>
            <?= $this->render(
                'content.php',
                ['content' => $content, 'directoryAsset' => $directoryAsset]
            )
            ?>
            <footer class="footer">
                <div class="container-fluid">
                    <p class="copyright">
                        &copy; <?= date('Y') ?> <a href="http://www.rekamy.com">TUDM Gong Kedak</a>
                    </p>
                </div>
            </footer>
        </div>
    </div>

    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
