<?php
use ramosisw\CImaterial\widgets\Menu;

?>
<div class="sidebar" data-color="purple" data-image="../assets/img/sidebar-1.jpg">
<!--
Tip 1: You can change the color of the sidebar using: data-color="purple | blue | green | orange | red"

Tip 2: you can also add an image using data-image tag
-->

<div class="logo">
    <a href="http://www.creative-tim.com" class="simple-text">
        E-KONTRAK
    </a>
</div>

<div class="sidebar-wrapper">
    <?php echo Menu::widget([
        'options'=>['class'=>'nav'],
        'items'=>[
            ['label' => 'Home', 'icon'=>'dashboard','url' => ['/site/index']],
            ['label' => 'Kontrak', 'icon'=>'library_books', 'url' => ['/contract/index']],
            ['label' => 'Geran', 'icon'=>'bubble_chart', 'url' => ['/contract-grant/index']],
            ['label' => 'Syarikat', 'icon'=>'work', 'url' => ['/company/index']],
            // ['label' => 'Permohonan', 'icon'=>'content_paste', 'url' =>'#','items' => [
            //     ['label' => 'Kontrak', 'icon'=>'library_books', 'url' => ['/contract/index']],
            //     ['label' => 'Geran', 'icon'=>'bubble_chart', 'url' => ['/contract-grant/index']],

            // ]],
            ['label' => 'Pentadbiran', 'icon'=>'settings', 'url' =>'#','items' => [
                //admin only
                ['label' => 'Setting', 'icon'=>'notifications', 'url' => ['/setting/index']],
                ['label' => 'Kod Bidang', 'icon'=>'unarchive', 'url' => ['/field-code/index']],
                ['label' => 'Sel', 'icon'=>'location_on', 'url' => ['/category/index']],
                ['label' => 'Pengguna', 'icon'=>'dashboard', 'url' => ['/user/index']],

                //user only
                ['label' => 'Profil', 'icon'=>'person', 'url' => ['/user/view','id'=>Yii::$app->user->id]],

            ]],
        ]
    ]
    ) ?>
</div>
</div>
