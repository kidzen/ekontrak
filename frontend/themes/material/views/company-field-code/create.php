<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\CompanyFieldCode */

$this->title = 'Create Company Field Code';
$this->params['breadcrumbs'][] = ['label' => 'Company Field Code', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-field-code-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
