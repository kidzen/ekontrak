<?php

namespace frontend\controllers;

use Yii;
use frontend\models\JobGrant;
use frontend\models\search\JobGrantSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * JobGrantController implements the CRUD actions for JobGrant model.
 */
class JobGrantController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all JobGrant models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new JobGrantSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single JobGrant model.
     * @param integer $job_id
     * @param integer $grant_id
     * @return mixed
     */
    public function actionView($job_id, $grant_id)
    {
        $model = $this->findModel($job_id, $grant_id);
        return $this->render('view', [
            'model' => $this->findModel($job_id, $grant_id),
        ]);
    }

    /**
     * Creates a new JobGrant model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new JobGrant();

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'job_id' => $model->job_id, 'grant_id' => $model->grant_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing JobGrant model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $job_id
     * @param integer $grant_id
     * @return mixed
     */
    public function actionUpdate($job_id, $grant_id)
    {
        $model = $this->findModel($job_id, $grant_id);

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'job_id' => $model->job_id, 'grant_id' => $model->grant_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing JobGrant model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $job_id
     * @param integer $grant_id
     * @return mixed
     */
    public function actionDelete($job_id, $grant_id)
    {
        $this->findModel($job_id, $grant_id)->deleteWithRelated();

        return $this->redirect(['index']);
    }

    
    /**
     * Finds the JobGrant model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $job_id
     * @param integer $grant_id
     * @return JobGrant the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($job_id, $grant_id)
    {
        if (($model = JobGrant::findOne(['job_id' => $job_id, 'grant_id' => $grant_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
