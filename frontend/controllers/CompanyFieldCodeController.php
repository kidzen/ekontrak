<?php

namespace frontend\controllers;

use Yii;
use frontend\models\CompanyFieldCode;
use frontend\models\search\CompanyFieldCodeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CompanyFieldCodeController implements the CRUD actions for CompanyFieldCode model.
 */
class CompanyFieldCodeController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all CompanyFieldCode models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CompanyFieldCodeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CompanyFieldCode model.
     * @param integer $code_id
     * @param integer $company_id
     * @return mixed
     */
    public function actionView($code_id, $company_id)
    {
        $model = $this->findModel($code_id, $company_id);
        return $this->render('view', [
            'model' => $this->findModel($code_id, $company_id),
        ]);
    }

    /**
     * Creates a new CompanyFieldCode model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CompanyFieldCode();

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'code_id' => $model->code_id, 'company_id' => $model->company_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing CompanyFieldCode model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $code_id
     * @param integer $company_id
     * @return mixed
     */
    public function actionUpdate($code_id, $company_id)
    {
        $model = $this->findModel($code_id, $company_id);

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'code_id' => $model->code_id, 'company_id' => $model->company_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing CompanyFieldCode model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $code_id
     * @param integer $company_id
     * @return mixed
     */
    public function actionDelete($code_id, $company_id)
    {
        $this->findModel($code_id, $company_id)->deleteWithRelated();

        return $this->redirect(['index']);
    }

    
    /**
     * Finds the CompanyFieldCode model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $code_id
     * @param integer $company_id
     * @return CompanyFieldCode the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($code_id, $company_id)
    {
        if (($model = CompanyFieldCode::findOne(['code_id' => $code_id, 'company_id' => $company_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
