<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'Sistem eKontrak Gong Kedak',
    'name' => 'Sistem eKontrak Gong Kedak',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    // 'bootstrap' => ['log','common\components\Settings'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'view' => [
            'theme' => [
                'pathMap' => [
                    // '@frontend/views' => '@frontend/themes/material/views',
                    '@frontend/views' => '@frontend/themes/adminlte/views',
                    // '@frontend/views' => '@frontend/views',
                    // '@frontend/views' => '@frontend/themes/gentella/views',
                    // '@frontend/views' => '@vendor/yiister/yii2-gentelella/views',
                    // '@frontend/views' => '@frontend/themes/sbadmin2/views',
                ],
            ],
        ],
        'notify' => [
            'class' => 'common\components\Notify',
        ],
        'request' => [
            'csrfParam' => '_csrf-frontend',
        ],
        // 'user' => [
        //     'identityClass' => 'common\models\User',
        //     'enableAutoLogin' => true,
        //     'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        // ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'ekontrak-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        /*
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        */
    ],
    'modules' => [
        'user' => [
            'class' => 'dektrium\user\Module',
            'enableUnconfirmedLogin' => true,
            'rememberFor' => false,
            'emailChangeStrategy' => \dektrium\user\Module::STRATEGY_DEFAULT,
            'enableAccountDelete' => false,
            'enablePasswordRecovery' => true,
            'enableUnconfirmedLogin' => false,
            'enableConfirmation' => true,
            'confirmWithin' => 21600,
            'cost' => 12,
            'admins' => ['admin']
        ],
        // If you use tree table
        'treemanager' =>  [
            'class' => '\kartik\tree\Module',
        ],
        'gridview' => [
            'class' => '\kartik\grid\Module',
        ],
        'datecontrol' => [
            'class' => '\kartik\datecontrol\Module',
        ],
    ],
    'params' => $params,
];
