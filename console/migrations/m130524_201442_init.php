<?php

// use yii\db\Migration;
use common\components\Migration;

class m130524_201442_init extends Migration
{
    public function up()
    {
        $this->customDrop();
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
        // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        // $rbac = new m140506_102106_rbac_init();
        // $rbac->up();

        $this->createTable('{{%role}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->unique(),
            'description' => $this->string(),

            'status' => $this->smallInteger()->defaultValue(1),
            'deleted' => $this->smallInteger()->defaultValue(0),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'deleted_at' => $this->timestamp(),
            'created_by' => $this->timestamp(),
            'updated_by' => $this->timestamp(),
            'deleted_by' => $this->timestamp(),
        ], $tableOptions);

        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull()->unique(),
            'role_id' => $this->integer()->notNull(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'email' => $this->string()->unique(),

            'status' => $this->smallInteger()->defaultValue(1),
            'deleted' => $this->smallInteger()->defaultValue(0),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'deleted_at' => $this->timestamp(),
            'created_by' => $this->timestamp(),
            'updated_by' => $this->timestamp(),
            'deleted_by' => $this->timestamp(),
        ], $tableOptions);

        $this->createTable('{{%settings}}', [
            'id' => $this->primaryKey(),
            'key' => $this->string()->notNull()->unique(),
            'value' => $this->string()->notNull(),
        ], $tableOptions);

        $this->createTable('{{%field_code}}', [
            'id' => $this->primaryKey(),
            'code' => $this->integer()->unique(),
            'name' => $this->string()->unique(),

            'status' => $this->smallInteger()->defaultValue(1),
            'deleted' => $this->smallInteger()->defaultValue(0),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'deleted_at' => $this->timestamp(),
            'created_by' => $this->timestamp(),
            'updated_by' => $this->timestamp(),
            'deleted_by' => $this->timestamp(),
        ], $tableOptions);

        $this->createTable('{{%company_field_code}}', [
            // 'id' => $this->primaryKey(),
            'code_id' => $this->integer(),
            'company_id' => $this->integer(),

            'status' => $this->smallInteger()->defaultValue(1),
            'deleted' => $this->smallInteger()->defaultValue(0),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'deleted_at' => $this->timestamp(),
            'created_by' => $this->timestamp(),
            'updated_by' => $this->timestamp(),
            'deleted_by' => $this->timestamp(),
        ], $tableOptions);

        $this->createTable('{{%company}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->unique(),

            'status' => $this->smallInteger()->defaultValue(1),
            'deleted' => $this->smallInteger()->defaultValue(0),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'deleted_at' => $this->timestamp(),
            'created_by' => $this->timestamp(),
            'updated_by' => $this->timestamp(),
            'deleted_by' => $this->timestamp(),
        ], $tableOptions);

        $this->createTable('{{%category}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->unique(),
            'description' => $this->string(),

            'status' => $this->smallInteger()->defaultValue(1),
            'deleted' => $this->smallInteger()->defaultValue(0),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'deleted_at' => $this->timestamp(),
            'created_by' => $this->timestamp(),
            'updated_by' => $this->timestamp(),
            'deleted_by' => $this->timestamp(),
        ], $tableOptions);

        $this->createTable('{{%sel}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->unique(),
            'description' => $this->string(),

            'status' => $this->smallInteger()->defaultValue(1),
            'deleted' => $this->smallInteger()->defaultValue(0),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'deleted_at' => $this->timestamp(),
            'created_by' => $this->timestamp(),
            'updated_by' => $this->timestamp(),
            'deleted_by' => $this->timestamp(),
        ], $tableOptions);

        $this->createTable('{{%contract}}', [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer(),
            'contract_no' => $this->string()->unique(),
            // 'indent_no' => $this->string(),
            'meeting_id' => $this->integer(),
            'revenue' => $this->double(),
            // 'limit' => $this->double(),
            'start_date' => $this->date(),
            'end_date' => $this->date(),

            'status' => $this->smallInteger()->defaultValue(1),
            'deleted' => $this->smallInteger()->defaultValue(0),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'deleted_at' => $this->timestamp(),
            'created_by' => $this->timestamp(),
            'updated_by' => $this->timestamp(),
            'deleted_by' => $this->timestamp(),
        ], $tableOptions);

        $this->createTable('{{%meeting}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->unique(),
            'timestamp' => $this->timestamp(),

            'status' => $this->smallInteger()->defaultValue(1),
            'deleted' => $this->smallInteger()->defaultValue(0),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'deleted_at' => $this->timestamp(),
            'created_by' => $this->timestamp(),
            'updated_by' => $this->timestamp(),
            'deleted_by' => $this->timestamp(),
        ], $tableOptions);

        $this->createTable('{{%job}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->unique(),
            'description' => $this->string(),
            'start_date' => $this->date(),
            'end_date' => $this->date(),

            'status' => $this->smallInteger()->defaultValue(1),
            'deleted' => $this->smallInteger()->defaultValue(0),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'deleted_at' => $this->timestamp(),
            'created_by' => $this->timestamp(),
            'updated_by' => $this->timestamp(),
            'deleted_by' => $this->timestamp(),
        ], $tableOptions);

        $this->createTable('{{%progress}}', [
            'id' => $this->primaryKey(),
            'job_id' => $this->integer(),
            'progress' => $this->double(),
            'cost' => $this->double(),
            'description' => $this->string(),
            'start_date' => $this->date(),
            'end_date' => $this->date(),

            'status' => $this->smallInteger()->defaultValue(1),
            'deleted' => $this->smallInteger()->defaultValue(0),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'deleted_at' => $this->timestamp(),
            'created_by' => $this->timestamp(),
            'updated_by' => $this->timestamp(),
            'deleted_by' => $this->timestamp(),
        ], $tableOptions);

        $this->createTable('{{%contract_grant}}', [
            'id' => $this->primaryKey(),
            'meeting_id' => $this->integer(),
            'contract_id' => $this->integer(),
            // 'meeting_id' => $this->integer(),
            'company_id' => $this->integer(),
            // 'job_id' => $this->integer(),
            'type' => $this->string(),
            'revenue' => $this->double(),
            'start_date' => $this->date(),
            'end_date' => $this->date(),

            'status' => $this->smallInteger()->defaultValue(1),
            'deleted' => $this->smallInteger()->defaultValue(0),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'deleted_at' => $this->timestamp(),
            'created_by' => $this->timestamp(),
            'updated_by' => $this->timestamp(),
            'deleted_by' => $this->timestamp(),
        ], $tableOptions);

        $this->createTable('{{%job_grant}}', [
            // 'id' => $this->primaryKey(),
            'job_id' => $this->integer(),
            'grant_id' => $this->integer(),

            'status' => $this->smallInteger()->defaultValue(1),
            'deleted' => $this->smallInteger()->defaultValue(0),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'deleted_at' => $this->timestamp(),
            'created_by' => $this->timestamp(),
            'updated_by' => $this->timestamp(),
            'deleted_by' => $this->timestamp(),
        ], $tableOptions);


        $i = 1;
        $this->addPrimaryKey('jt'.$i++, '{{job_grant}}', ['job_id', 'grant_id']);
        $this->addPrimaryKey('jt'.$i++, '{{company_field_code}}', ['company_id', 'code_id']);
        $i = 1;
        $this->addForeignKey('fk'.$i++,'{{user}}','role_id','{{role}}','id');
        $this->addForeignKey('fk'.$i++,'{{company_field_code}}','company_id','{{company}}','id');
        $this->addForeignKey('fk'.$i++,'{{company_field_code}}','code_id','{{field_code}}','id');
        $this->addForeignKey('fk'.$i++,'{{contract}}','category_id','{{category}}','id');
        $this->addForeignKey('fk'.$i++,'{{contract}}','meeting_id','{{meeting}}','id');
        // $this->addForeignKey('fk'.$i++,'{{contract_grant}}','meeting_id','{{meeting}}','id');
        // $this->addForeignKey('fk'.$i++,'{{job_field_code}}','job_id','{{job}}','id');
        // $this->addForeignKey('fk'.$i++,'{{job_field_code}}','code_id','{{field_code}}','id');
        $this->addForeignKey('fk'.$i++,'{{job_grant}}','job_id','{{job}}','id');
        $this->addForeignKey('fk'.$i++,'{{job_grant}}','grant_id','{{contract_grant}}','id');
        $this->addForeignKey('fk'.$i++,'{{contract_grant}}','meeting_id','{{meeting}}','id');
        $this->addForeignKey('fk'.$i++,'{{contract_grant}}','contract_id','{{contract}}','id');
        $this->addForeignKey('fk'.$i++,'{{contract_grant}}','company_id','{{company}}','id');
        $this->addForeignKey('fk'.$i++,'{{contract_grant}}','meeting_id','{{meeting}}','id');
        $this->addForeignKey('fk'.$i++,'{{company_field_code}}','company_id','{{company}}','id');
        $this->addForeignKey('fk'.$i++,'{{company_field_code}}','code_id','{{field_code}}','id');
        // $this->addForeignKey('fk'.$i++,'{{progress}}','job_id','{{job}}','id');
        // $this->addForeignKey('fk'.$i++,'{{payment}}','progress_id','{{progress}}','id');

        $i = 1;
        $this->createIndex('idx'.$i++,'{{user}}','username','unique');

        $this->insertDefaultData();

    }

    public function down()
    {
        $this->customDrop(true);
    }

    protected function insertDefaultData() {
        $i = 1;
        $this->batchInsert('{{%settings}}', ['id','key','value'],
            [
                [$i++,'simplifiedVersion',0,],
                // [$i++,'templateDirectory','originalTemplate/template/',],
                // [$i++,'minPasswordLength',4,],
                // [$i++,'minStaffNoLength',4,],
                // [$i++,'lateDraftWarningInDays',30,],
                // [$i++,'departmentAlterLimit',2,],
                // [$i++,'draftParamRequired',0,],
                // [$i++,'defaultJuuStaffAssigned',7750,],
            ]
        );

        $i = 1;
        $this->batchInsert('{{%role}}', ['id','name'],
            [
                [$i++,'Super Admin',],
                [$i++,'Account',],
                [$i++,'Sel Admin',],
                [$i++,'Sel User',],
                [$i++,'Kontraktor',],
            ]
        );

        $i = 1;
        $r = 1;
        $this->batchInsert('{{%user}}', ['id','username','password_hash','role_id','email','auth_key'],
            [
                [$i++,$r++.$i,Yii::$app->security->generatePasswordHash('user'),$r,$r.$i.'@mail.com',Yii::$app->security->generateRandomString()],
                [$i++,$r++.$i,Yii::$app->security->generatePasswordHash('user'),$r,$r.$i.'@mail.com',Yii::$app->security->generateRandomString()],
                [$i++,$r++.$i,Yii::$app->security->generatePasswordHash('user'),$r,$r.$i.'@mail.com',Yii::$app->security->generateRandomString()],
                [$i++,$r++.$i,Yii::$app->security->generatePasswordHash('user'),$r,$r.$i.'@mail.com',Yii::$app->security->generateRandomString()],
                [$i++,$r++.$i,Yii::$app->security->generatePasswordHash('user'),$r,$r.$i.'@mail.com',Yii::$app->security->generateRandomString()],
            ]
        );

        $i = 1;
        $this->batchInsert('{{%category}}', ['name'],
            [
                ['Sel A',],
                ['Sel B',],
            ]
        );

        $i = 1;
        $this->batchInsert('{{%company}}', ['name'],
            [
                ['Company A',],
                ['Company B',],
            ]
        );

        $i = 1;
        $this->batchInsert('{{%field_code}}', ['code','name'],
            [
                [$i.$i++,'Field A',],
                [$i.$i++,'Field B',],
            ]
        );

        $i = 1;
        $this->batchInsert('{{%job}}', ['name'],
            [
                ['Job A',],
                ['Job B',],
                ['Job C',],
                ['Job D',],
            ]
        );

        $i = 1;
        $this->batchInsert('{{%meeting}}', ['title'],
            [
                ['Meeting A',],
                ['Meeting B',],
            ]
        );

        // $r = mt_rand(1000,10000);
        $i = 1;
        $this->batchInsert('{{%contract}}', ['category_id','contract_no','meeting_id','revenue'],
            [
                [1,'Contract A',1,$r = mt_rand(1000,10000)],
                [2,'Contract B',1,$r = mt_rand(1000,10000)],
                [2,'Contract C',2,$r = mt_rand(1000,10000)],
            ]
        );

        // grant
        $i = 0;
        // while($r > 100) {
        for($i=0;$i < 20;$i++) {
            $rand = mt_rand(1,500);
            $this->batchInsert('{{%contract_grant}}', ['meeting_id','contract_id','company_id','revenue'],
                [
                    [mt_rand(1,2),mt_rand(1,3),mt_rand(1,2),$rand],
                ]
            );
            // $r = $r - $rand;
            // $i++;
        }

        //asign job
        $this->batchInsert('{{%job_grant}}', ['job_id','grant_id'],
            [
                [1,1],
                [2,1],
                [3,2],
                [3,3],
                [4,3],
            ]
        );

        //add progress
        $i = 0;
        // while($r > 100) {
        for($i=0;$i < 5;$i++) {
            $rand = mt_rand(1,30);
            $this->batchInsert('{{%progress}}', ['job_id','progress','cost','description'],
                [
                    [1,20,$rand,'Part '.$i],
                    [2,10,$rand,'Part '.$i],
                    [3,10,$rand,'Part '.$i],
                    [4,15,$rand,'Part '.$i],
                ]
            );
            // $r = $r - $rand;
            // $i++;
        }

    }
    protected function insertScenario()
    {
    }
}
